import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { useMutation, useQuery } from 'react-query'

interface Todo {
  id: number;
  isMarked: boolean;
  description: string;
}

const markTodo = (v: { id: number, isMarked: boolean }): Promise<Response> => {
  const fd = new FormData()
  fd.append("is-marked", v.isMarked ? "true" : "false")

  return fetch(`http://localhost:3000/todos/${v.id}`, { method: "POST", body: fd })
}

const addTodo = (v: { description: string }): Promise<Response> => {
  const fd = new FormData()
  fd.append("description", v.description)

  return fetch("http://localhost:3000/todos", { method: "POST", body: fd })
}

function App() {
  const [count, setCount] = useState(0)
  const { data, isLoading, refetch } = useQuery<Todo[]>("todos", () => fetch("http://localhost:3001/todos").then(res => res.json()))
  const { mutate: sendMarkTodo } = useMutation(markTodo)
  const { mutate: sendAddTodo } = useMutation(addTodo)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <ul style={{ listStyle: "none" }}>
        {data && (
          data.map(todo => (
            <li style={{ textAlign: "left" }} key={todo.id}>
              <label>
                <input
                  defaultChecked={todo.isMarked}
                  onChange={ev => sendMarkTodo({ id: todo.id, isMarked: ev.target.checked })}
                  type="checkbox"
                />
                &nbsp;{todo.description}
              </label>
            </li>
          ))
        )}
      </ul>
      <form onSubmit={ev => {
        ev.preventDefault()
        const inp = (ev.target as unknown as { "new-todo": HTMLInputElement })["new-todo"]
        if (inp)
          sendAddTodo({ description: inp.value }, { onSuccess: () => refetch() })
      }}>
        <label>New todo:
          <input name="new-todo" type="text" />
        </label>
        <button type="submit" hidden />
      </form>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App
