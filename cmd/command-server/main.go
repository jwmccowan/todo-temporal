package main

import (
	"context"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/jwmccowan/todo-temporal/app"
	"go.temporal.io/sdk/client"
)

func AddTodo(description string) (int, error) {
	c, err := client.Dial(client.Options{})
	if err != nil {
		return 0, err
	}
	defer c.Close()

	options := client.StartWorkflowOptions{
		ID:        "todo-workflow",
		TaskQueue: app.TodoQueue,
	}

	we, err := c.ExecuteWorkflow(context.Background(), options, app.AddTodoWorkflow, description)
	if err != nil {
		return 0, err
	}

	var id int
	err = we.Get(context.Background(), &id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func MarkTodo(id int, mark bool) (int, error) {
	c, err := client.Dial(client.Options{})
	if err != nil {
		return 0, err
	}
	defer c.Close()

	options := client.StartWorkflowOptions{
		ID:        "todo-workflow",
		TaskQueue: app.TodoQueue,
	}

	we, err := c.ExecuteWorkflow(context.Background(), options, app.MarkTodoWorkflow, id, mark)
	if err != nil {
		return 0, err
	}

	var returnId int
	err = we.Get(context.Background(), &returnId)
	if err != nil {
		return 0, err
	}

	return returnId, nil
}

func main() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:5173"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, world!")
	})
	e.POST("/todos", func(c echo.Context) error {
		description := c.FormValue("description")
		id, err := AddTodo(description)
		if err != nil {
			c.String(500, "Something went wrong")
		}
		return c.String(http.StatusOK, strconv.Itoa(id))
	})
	e.POST("/todos/:id", func(c echo.Context) error {
		idParam := c.Param("id")
		id, err := strconv.Atoi(idParam)
		if err != nil {
			c.String(500, "Something went wrong")
		}
		isMarkedParam := c.FormValue("is-marked")
		id, err = MarkTodo(id, isMarkedParam == "true")
		if err != nil {
			c.String(500, "Something went wrong")
		}
		return c.String(http.StatusOK, strconv.Itoa(id))
	})
	e.Logger.Fatal(e.Start(":3000"))
}
