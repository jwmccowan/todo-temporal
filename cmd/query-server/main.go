package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/jwmccowan/todo-temporal/db"
)

type Todo struct {
	Id          int    `json:"id" xml:"id"`
	IsMarked    bool   `json:"isMarked" xml:"is_marked"`
	Description string `json:"description" xml:"description"`
}

func main() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:5173"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, world!")
	})
	e.GET("/todos", func(c echo.Context) error {
		db, err := db.GetDbConnection()
		if err != nil {
			c.String(500, "Something went wrong")
		}
		defer db.Close()
		rows, err := db.Query("select id, is_checked, description FROM todo order by id")
		defer rows.Close()

		var todos []Todo
		for rows.Next() {
			var id int
			var is_marked bool
			var description string
			err = rows.Scan(&id, &is_marked, &description)
			if err != nil {
				c.String(500, "Something went wrong")
			}
			todo := Todo{Id: id, IsMarked: is_marked, Description: description}
			fmt.Println(fmt.Sprintf("%+v", todos))
			todos = append(todos, todo)
		}
		return c.JSON(http.StatusOK, todos)
	})
	e.Logger.Fatal(e.Start(":3001"))
}
