package main

import (
	"log"

	"gitlab.com/jwmccowan/todo-temporal/app"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

func main() {
	c, err := client.Dial(client.Options{})
	if err != nil {
		log.Fatalln("unable to create Temporal client", err)
	}
	defer c.Close()

	w := worker.New(c, app.TodoQueue, worker.Options{})
	w.RegisterWorkflow(app.AddTodoWorkflow)
	w.RegisterActivity(app.AddTodo)
	w.RegisterWorkflow(app.MarkTodoWorkflow)
	w.RegisterActivity(app.MarkTodo)

	err = w.Run(worker.InterruptCh())
	if err != nil {
		log.Fatalln("unable to start worker", err)
	}
}
