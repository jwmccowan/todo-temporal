package app

import (
	"time"

	"go.temporal.io/sdk/workflow"
)

func AddTodoWorkflow(ctx workflow.Context, description string) (int, error) {
	options := workflow.ActivityOptions{
		StartToCloseTimeout: time.Second * 5,
	}

	ctx = workflow.WithActivityOptions(ctx, options)

	var id int
	err := workflow.ExecuteActivity(ctx, AddTodo, description).Get(ctx, &id)

	return id, err
}

func MarkTodoWorkflow(ctx workflow.Context, id int, mark bool) (int, error) {
	options := workflow.ActivityOptions{
		StartToCloseTimeout: time.Second * 5,
	}

	ctx = workflow.WithActivityOptions(ctx, options)

	var returnId int
	err := workflow.ExecuteActivity(ctx, MarkTodo, id, mark).Get(ctx, &returnId)

	return returnId, err
}
