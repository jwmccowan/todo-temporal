package app

import (
	"context"
	"fmt"

	_ "github.com/lib/pq"
	"gitlab.com/jwmccowan/todo-temporal/db"
)

func AddTodo(ctx context.Context, description string) (int, error) {
	db, err := db.GetDbConnection()
	if err != nil {
		return 0, err
	}
	defer db.Close()
	sqlStatement := `
		insert into todo(is_checked, description)
		values ($1, $2)
		returning id
	`
	id := 0
	err = db.QueryRow(sqlStatement, false, description).Scan(&id)
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func MarkTodo(ctx context.Context, id int, mark bool) (int, error) {
	db, err := db.GetDbConnection()
	if err != nil {
		return 0, err
	}
	defer db.Close()
	sqlStatement := `
		update todo
		set is_checked = $1
		where id = $2
	`
	res, err := db.Exec(sqlStatement, mark, id)
	if err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	if count != 1 {
		return 0, fmt.Errorf("Did not update a row for id %d", id)
	}

	return int(id), nil
}
